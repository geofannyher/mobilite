import 'mapbox-gl/dist/mapbox-gl.css';
import './dashboardStyle.css'
import { Center } from '@chakra-ui/layout';
import {
    MapContainer,
    TileLayer,
    Polygon,
    GeoJSON
} from 'react-leaflet';
import { statesData } from './data';
import { jutaraData } from '../../data/js/jakarta-utara-rw.js';
import 'leaflet/dist/leaflet.css'
import { useRef } from 'react';

import { useDispatch } from 'react-redux'
import { changeLocationOfInterestValue } from '../../redux/features/yourHomeLocationCard/locationOfInterestCardSlice';

const Dashboard = () => {
    const dispatch = useDispatch();

    const zoom = '10'
    const style = {
        marginTop: '1px',
        paddingTop: '5px',
        width: '1200px',
        height: "850px",
        zIndex: 1
    }
    const setView = [-6.200000, 106.816666]

    const geoJsonRef = useRef();

    let startCoords = 0;
    let startLayer;
    let home_loc = "";
    let home_loc_hCost = 0;

    // get color depending on 'T transit' value
    function getColor(d) {
        return d > 13000 ? '#08589e' :
          d > 11000 ? '#2b8cbe' :
            d > 9000 ? '#4eb3d3' :
              d > 7000 ? '#7bccc4' :
                d > 5000 ? '#a8ddb5' :
                  d > 3000 ? '#ccebc5' :
                    d > 1000 ? '#e0f3db' : '#f7fcf0';
      }

    function geoJSONStyle(feature) {
        return {
          weight: 1,
          opacity: 1,
          //color: 'white',
          color: '#666',  /* #666 - dark grey */
          dashArray: '3',
          fillOpacity: 0.7,
          fillColor: getColor(feature.properties["T transit"])
        };
      }

    function highlightFeature(e) {
        const layer = e.target;
  
        const restorefillColor = e.target.options.fillColor;
        // console.log("highLight: ", e.target.options.fillColor)

        dispatch(changeLocationOfInterestValue({
            WADMKK: e.target.feature.properties.WADMKK,
            WADMKC: e.target.feature.properties.WADMKC,
            WADMKD: e.target.feature.properties.WADMKD,
            WADMRW: e.target.feature.properties.WADMRW,
            from_origin: "",
            home_loc: "",
        }));
  
        // info.update(layer.feature.properties);
  
        if (layer === startLayer) return;
  
        layer.setStyle({
            weight: 5,
            color: 'blue',
            dashArray: '',
            fillOpacity: 0
        });
  
        layer.bringToFront();
      }

      function resetHighlight(e) {

        // console.log("resetLight 1: ", e.target.options.fillColor)
        if (e.target.feature.properties.OBJECTID == startCoords) {
          // do nothing
        } else {
          e.target.setStyle({
            weight: 1,
            opacity: 1,
            color: '#666',  /* #666 - dark grey */
            dashArray: '3',
            fillOpacity: 0.7,
          });
          //geojson_jp.setStyle(restoreFillColor);
          //geojson_js.setStyle(restoreFillColor);
  
        }
        // console.log("resetLight 2: ", e.target.options.fillColor)
  
        // info.update();
      }

      function recordHomeWorkLocations(e) {
        const coordinates = e.latlng.lat + ',' + e.latlng.lng;
        // console.log(e, "dari fiture")
        console.log(
            e.target.feature.properties.WADMKK, 
            e.target.feature.properties.WADMKC, 
            e.target.feature.properties.WADMKD, 
            e.target.feature.properties.WADMRW,
            home_loc_hCost, "data")
  
        // clear previous highlight
        // geoJsonRef.current.leafletElement.resetStyle(e.target);
        // geojson_jp.resetStyle(startLayer);
        // geojson_js.resetStyle(startLayer);
        // geojson_jb.resetStyle(startLayer);
        // geojson_jt.resetStyle(startLayer);
        // geojson_ju.resetStyle(startLayer);
  
        startCoords = 0;
  
        startCoords = e.target.feature.properties.OBJECTID;
        startLayer = e.target;
        home_loc = e.target.feature.properties.WADMKD;
        // home_loc_hCost = gethCost(e.target.feature.properties.WADMKK);
        //console.log("recordHome :", e.target.feature.properties.from_origin)
        
        // directions.innerHTML = `Your home location</br>
        //     <b>${e.target.feature.properties.WADMKK}</b></br>
        //     <b>${e.target.feature.properties.WADMKC}</b></br>
        //     ${e.target.feature.properties.WADMKD} RW ${e.target.feature.properties.WADMRW}</br></br>
        //     <b>Est. H cost:</b></br>
        //     Housing: IDR ${home_loc_hCost} per month</br>
        //     <b>Est. T accessiblity score: </b><i>no data</i></b></br>`;
  
        if (startCoords) {
        //   updateLayerColor();
        //   highlightHomeLocation();
        }
      }

    function onEachFeature(feature, layer) {
        layer.on({
          mouseover: highlightFeature,
          mouseout: resetHighlight,
          /*click: zoomToFeature */
          click: recordHomeWorkLocations
        });
      }

    return (
        <div className='baseMap'>
            <Center >
                <MapContainer
                    zoom={zoom}
                    scrollWheelZoom={false}
                    style={style}
                    center={setView}
                >
                    <TileLayer
                        url="https://api.maptiler.com/maps/streets-v2/256/{z}/{x}/{y}.png?key=nT4xO7QuKRinvTFEebNJ"
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    />

                    
                    {/* data  */}
                    {/* {statesData.features.map((state) => {
                        const cordinate = state.geometry.coordinates[0].map((item) => [item[1], item[0]]);
                        return (
                            <Polygon
                                pathOptions={{
                                    fillColor: '#FD8D',
                                    fillOpacity: 0.7,
                                    wight: 2,
                                    opacity: 1,
                                    dashArray: 3,
                                    color: 'white'
                                }}
                                positions={cordinate}
                                eventHandlers={{
                                    mouseover: (e) => {
                                        const layer = e.target;
                                        layer.setStyle({
                                            fillOpacity: 0.7,
                                            wight: 5,
                                            dashArray: "",
                                            color: '#666',
                                            fillColor: 'red'
                                        })
                                    },
                                    mouseout: (e) => {
                                        const layer = e.target;
                                        layer.setStyle({
                                            fillOpacity: 0.7,
                                            wight: 2,
                                            dashArray: 3,
                                            color: 'white'
                                        })
                                    }
                                }}
                            />
                        )
                    })} */}
                    
                    {/* {jutaraData.features.map((state) => {
                        console.log(state, "state");
                        const cordinate = state.geometry.coordinates[0].map((item) => [item[1], item[0]]);
                        console.log(cordinate, "cordinate");
                        return (
                            <Polygon
                                pathOptions={{
                                    fillColor: '#FD8D',
                                    fillOpacity: 0.7,
                                    wight: 2,
                                    opacity: 1,
                                    dashArray: 3,
                                    color: 'white'
                                }}
                                positions={cordinate}
                                eventHandlers={{
                                    mouseover: (e) => {
                                        const layer = e.target;
                                        layer.setStyle({
                                            fillOpacity: 0.7,
                                            wight: 5,
                                            dashArray: "",
                                            color: '#666',
                                            fillColor: 'red'
                                        })
                                    },
                                    mouseout: (e) => {
                                        const layer = e.target;
                                        layer.setStyle({
                                            fillOpacity: 0.7,
                                            wight: 2,
                                            dashArray: 3,
                                            color: 'white'
                                        })
                                    }
                                }}
                            />
                        )
                    })} */}
                    <GeoJSON data={jutaraData} style={geoJSONStyle} onEachFeature={onEachFeature} ref={geoJsonRef} />
                </MapContainer>
            </Center>

        </div>
    )
}

export default Dashboard
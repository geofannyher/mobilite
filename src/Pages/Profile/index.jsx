import { Image,InputGroup, InputLeftElement, Box, Tabs, TabList, TabPanels, Tab, TabPanel, Text, Switch, Flex, Input, IconButton } from '@chakra-ui/react';
import { useState } from 'react';
import { SearchIcon } from '@chakra-ui/icons';

import MRT from'./Gambar/MRT.png';
import BRT from'./Gambar/BRT.png';
import LRT from'./Gambar/LRT.png';
import Microtrans from'./Gambar/Microtrans.png';
import Otherbusroutes from'./Gambar/Otherbusroutes.png';
import Walking from'./Gambar/Walking.png';

const MyCard = () => {
  const maxHeightInCm = 7;
  const maxHeightInPx = `${maxHeightInCm * 37.795276}px`;

  const [isMRTSelected, setIsMRTSelected] = useState(false);
  const [isBRTSelected, setIsBRTSelected] = useState(false);
  const [isLRTSelected, setIsLRTSelected] = useState(false);
  const [isMicrotransSelected, setIsMicrotransSelected] = useState(false);
  const [isOtherBusRoutesSelected, setIsOtherBusRoutesSelected] = useState(false);
  const [isOtherWalkingSelected, setIsOtherWalkingSelected] = useState(false);

  const [searchQuery, setSearchQuery] = useState("");

  const handleSearchInputChange = (event) => {
    setSearchQuery(event.target.value);
  }

  return (
    <>
      <Box h={331} bg="white" borderRadius={20} boxShadow="0px 4px 10px rgba(0, 0, 0, 0.1)">
        <Tabs>
          <TabList flex={1}>
            <Tab flexGrow={1}>Dashboard</Tab>
            <Tab flexGrow={1}>Data</Tab>
          </TabList>
          <TabPanels>
            <TabPanel>
              <Box p={4} overflowY="auto" maxHeight={maxHeightInPx}>
                <Flex alignItems="center">
                  <InputGroup boxShadow="0px 4px 25px #F5F5F5">
                    <InputLeftElement>
                        {<SearchIcon color="black.300" />}
                    </InputLeftElement>
                    <Input
                      value={searchQuery}
                      onChange={handleSearchInputChange}
                      placeholder="Search dashboard"
                      mr={4}
                    />
                  </InputGroup>
                </Flex>

                {searchQuery.trim() === "" ? (
                  <>
                    <Text>Content for Tab 1 goes hereas</Text>
                    <br/>
                    <Text>Another content for Tab 1 goes here</Text>
                  </>
                ) : (
                  <>
                    {["Content for Tab 1 goes hereas", "Another content for Tab 1 goes here"].map((content) => {
                      if (content.toLowerCase().includes(searchQuery.toLowerCase())) {
                        return (
                          <Text key={content}>{content}</Text>
                        );
                      }
                      return null;
                    })}
                  </>
                )}
              </Box>
            </TabPanel>
            <TabPanel>
              <Box p={4} overflowY="auto" maxHeight={maxHeightInPx}>
                <Flex alignItems="center">
                  <InputGroup boxShadow="0px 4px 25px #F5F5F5">
                    <InputLeftElement>
                      {<SearchIcon color="black.300" />}
                    </InputLeftElement>
                    <Input
                      value={searchQuery}
                      onChange={handleSearchInputChange}
                      placeholder="Search data"
                      mr={4}
                    />
                  </InputGroup>
                </Flex>

                {searchQuery.trim() === "" ? (
                  <>
                    <Text>Content for Tab 2 goes here</Text>
                    <br/>
                    <Text>Another content for Tab 2 goes here</Text>
                    <br/>
                  </>
                ) : (
                  <>
                    {["Content for Tab 2 goes here", "Another content for Tab 2 goes here"].map((content) => {
                      if (content.toLowerCase().includes(searchQuery.toLowerCase())) {
                        return (
                          <Text key={content}>{content}</Text>
                        );
                      }
                      return null;
                    })}
                  </>
                )}
              </Box>
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Box>
      <Box bg="white" borderRadius={20} boxShadow="0px 4px 10px rgba(0, 0, 0, 0.1)">
        <Box p={4}>
          <Text fontWeight="bold" fontSize="xl" mb={4} ml={4}>
            Choose Mode
          </Text>

          <Box p={4} display="flex" flexDirection="column" gap="10px">
            <Flex alignItems="center" justifyContent="space-around" gap="5px">
              <Image src={MRT} alt="MRT" boxSize="30px" width="1.1cm" height="0.3cm" />
              <Text>MRT</Text>
              <Switch size="md" isChecked={isMRTSelected} onChange={() => setIsMRTSelected(!isMRTSelected)} ml="auto" />
            </Flex>
            <Flex alignItems="center" justifyContent="space-around" gap="5px">
              <Image src={BRT} alt="BRT" boxSize="30px" width="1.1cm" height="0.3cm" />
              <Text>BRT</Text>
              <Switch size="md" isChecked={isBRTSelected} onChange={() => setIsBRTSelected(!isBRTSelected)} ml="auto" />
            </Flex>
            <Flex alignItems="center" justifyContent="space-around" gap="5px">
              <Image src={LRT} alt="LRT" boxSize="30px" width="1.1cm" height="0.3cm" />
              <Text>LRT</Text>
              <Switch size="md" isChecked={isLRTSelected} onChange={() => setIsLRTSelected(!isLRTSelected)} ml="auto" />
            </Flex>
            <Flex alignItems="center" justifyContent="space-around" gap="5px">
              <Image src={Microtrans} alt="Microtrans" boxSize="30px" width="1.1cm" height="0.3cm" />
              <Text>Microtrans</Text>
              <Switch size="md" isChecked={isMicrotransSelected} onChange={() => setIsMicrotransSelected(!isMicrotransSelected)} ml="auto" />
            </Flex>
            <Flex alignItems="center" justifyContent="space-around" gap="5px">
              <Image src={Otherbusroutes} alt="Other Bus Routes" boxSize="30px" width="1.1cm" height="0.3cm" />
              <Text>Other Bus Routes</Text>
              <Switch size="md" isChecked={isOtherBusRoutesSelected} onChange={() => setIsOtherBusRoutesSelected(!isOtherBusRoutesSelected)} ml="auto" />
            </Flex>
            <Flex alignItems="center" justifyContent="space-around" gap="5px">
              <Image src={Walking} alt="Walking" boxSize="30px" width="1.1cm" height="0.3cm" />
              <Text>Walking</Text>
              <Switch size="md" isChecked={isOtherWalkingSelected} onChange={() => setIsOtherWalkingSelected(!isOtherWalkingSelected)} ml="auto" />
            </Flex>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default MyCard;

import {
  Box,
  Center,
  Button,
  Text,
  Badge,
  Grid,
  GridItem,
} from "@chakra-ui/react";
import React, { useRef } from "react";
import { useReactToPrint } from "react-to-print";
import HomeLoc from "../../Component/Layout/cards/HomeLoc";
import LocOfInterest from "../../Component/Layout/cards/LocOfInterest";
import Dashboard from "../Dashboard/dashboard";
import MyCard from "../Profile";

const Hero = () => {
  const componentprint = useRef();
  const handlePrint = useReactToPrint({
    content: () => componentprint.current,
    documentTitle: "save",
    onAfterPrint: () => console.log(""),
  });
  return (
    <Box bg="#E6ECF1">
      <Box display="flex" justifyContent="space-around" pt="10px">
        <Box bg={"#E6ECF1"} display="flex" flexDirection="column" gap="10px">
          <MyCard />
        </Box>
        {/* <div ref={componentprint}>
          <Box px={5} py={5}>
            <Text>
              testing
              <Badge ml="1" colorScheme="green">
                New
              </Badge>
            </Text>
          </Box>
        </div>
        <Button size="sm" colorScheme="teal" onClick={handlePrint}>
          print
        </Button> */}
        <Box bg={"#E6ECF1"} display="flex" flexDirection="column" gap="10px">
          <Dashboard />
        </Box>
        <Box bg={"#E6ECF1"} display="flex" flexDirection="column" gap="10px">
          <HomeLoc />
          <LocOfInterest />
        </Box>
      </Box>
    </Box>
  );
};

export default Hero;

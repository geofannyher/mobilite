import React from "react";
import { useHistory } from "react-router-dom";

const Logout = () => {
  const history = useHistory();

  const handleLogout = () => {
    // menghapus data login dari local storage atau session storage jika menggunakan itu
    localStorage.removeItem("userData");
    // mengarahkan pengguna kembali ke form login
    history.push("/login");
  };

  return (
    <button onClick={handleLogout}>
      CgLogOut
    </button>
  );
};

export default Logout;
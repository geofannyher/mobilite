import { useState } from "react";
import {
  Box,
  FormControl,
  FormLabel,
  Input,
  Checkbox,
  Stack,
  Link,
  Button,
  InputGroup,
  InputRightElement,
  Flex,
  Heading,
  useToast,
} from "@chakra-ui/react";
import { FaEye, FaEyeSlash } from "react-icons/fa";
import axios from "axios";
import { useNavigate } from 'react-router-dom'

const LoginForm = () => {
  const navigate = useNavigate();
  const toast = useToast();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [remember, setRemember] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

  const handleEmailChange = (e) => setEmail(e.target.value);
  const handlePasswordChange = (e) => setPassword(e.target.value);
  const handleRememberChange = (e) => setRemember(e.target.checked);
  const handleShowPasswordClick = () => setShowPassword(!showPassword);

  const handleSubmit = async () => {
    try {
      const res = await axios.post(`${process.env.REACT_APP_BASE_URL}/login`, {
        email: email,
        password: password,
      });
      if (res.status === 200) {
        sessionStorage.setItem("data", JSON.stringify(res.data));
        navigate("/");
        toast({
          title: "Login Success",
          description: "You have successfully logged in.",
          status: "success",
          duration: 3000,
          isClosable: true,
        });
      }
    } catch (err) {
      if (err.response.status === 400) {
        toast({
          title: "Login Failed",
          description: "Invalid email or password.",
          status: "error",
          duration: 3000,
          isClosable: true,
        });
      }
    }
  };

  return (
    <Box
      p={6}
      maxW={"md"}
      borderWidth={1}
      borderRadius={8}
      boxShadow={"lg"}
      bg="white"
      position="absolute"
      top="50%"
      left="50%"
      transform="translate(-50%, -50%)"
    >
      <form
        onSubmit={(e) => {
          handleSubmit()
          e.preventDefault()
        }}>
        <Stack spacing={4}>
          <FormControl>
            <Heading as="h2" size="lg" mb="4">
              Sign In
            </Heading>
            <FormLabel>Email / username</FormLabel>
            <Input
              type="email"
              value={email}
              onChange={handleEmailChange}
              placeholder=""
            />
          </FormControl>
          <FormControl>
            <FormLabel>Password</FormLabel>
            <InputGroup>
              <Input
                type={showPassword ? "text" : "password"}
                value={password}
                onChange={handlePasswordChange}
                placeholder=""
              />
              <InputRightElement>
                {showPassword ? (
                  <FaEyeSlash onClick={handleShowPasswordClick} />
                ) : (
                  <FaEye onClick={handleShowPasswordClick} />
                )}
              </InputRightElement>
            </InputGroup>
          </FormControl>
          <Stack spacing={8}>
            <Flex justifyContent="space-between">
              <Checkbox
                isChecked={remember}
                onChange={handleRememberChange}
                colorScheme="green"
              >
                Remember me
              </Checkbox>
              <Link href="#">forget password ?</Link>
            </Flex>
            <Button type="submit" colorScheme="blue" size="lg" marginTop="2em">
              Login
            </Button>
          </Stack>
        </Stack>
      </form>
    </Box>
  );
};

const LoginPage = () => {
  return (
    <Box
      minH="100vh"
      bgGradient="linear(to-b, #0077B6, #1C3144)"
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      <LoginForm />
    </Box>
  );
};

export default LoginPage;
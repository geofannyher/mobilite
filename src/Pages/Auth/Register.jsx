import React, { useState } from "react";
import {
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  InputRightElement,
  Button,
  Box,
  Center,
  Heading,
  Card,
  useToast,
} from "@chakra-ui/react";
import { AiOutlineEye, AiOutlineEyeInvisible } from "react-icons/ai";
import axios from "axios";
import { useNavigate } from 'react-router-dom';

const Register = () => {
  const toast = useToast();
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const navigate = useNavigate()

  const handleEmailChange = (e) => { setEmail(e.target.value) }
  const handlePasswordChange = (e) => { setPassword(e.target.value) };
  const handleConfirmPasswordChange = (e) => { setConfirmPassword(e.target.value) };

  const handleSubmit = async () => {
    await axios
      .post(`${process.env.REACT_APP_BASE_URL}/register`, {
        email: email,
        password: password,
        confirmPassword: confirmPassword
      })
      .then((res) => {
        if (res.status === 200) {
          sessionStorage.setItem("data", JSON.stringify(res.data));
          navigate("/");
          toast({
            title: 'Register Success',
            status: 'success',
            duration: 3000,
            isClosable: true,
          })
        }
      })
      .catch((err) => {
        if (err.response.status === 400) {
          console.log(err.response.data);
        }
        toast({
          title: 'Regiser Error',
          status: 'error',
          duration: 3000,
          isClosable: true,
        })
      });
  }

  return (
    <Center h="100vh">
      <Box w="400px">
        <Card boxShadow="md" borderRadius="lg" p="4">
          <Heading size="lg" mb="4">
            Create an Account
          </Heading>
          <form
            onSubmit={(e) => {
              handleSubmit()
              e.preventDefault()
            }}>
            <FormControl id="email" mb="4">
              <FormLabel>Email</FormLabel>
              <Input type="email" value={email} onChange={handleEmailChange} width="100%" placeholder="Email" />
            </FormControl>
            <FormControl id="password" mb="4">
              <FormLabel>Password</FormLabel>
              <InputGroup>
                <Input
                  type={showPassword ? "text" : "password"}
                  value={password}
                  required
                  onChange={handlePasswordChange}
                  width="100%"
                  placeholder="Password"
                />
                <InputRightElement>
                  {showPassword ? (
                    <AiOutlineEyeInvisible onClick={() => setShowPassword(false)} />
                  ) : (
                    <AiOutlineEye onClick={() => setShowPassword(true)} />
                  )}
                </InputRightElement>
              </InputGroup>
            </FormControl>
            <FormControl id="confirmPassword" mb="4">
              <FormLabel>Confirm Password</FormLabel>
              <InputGroup>
                <Input
                  type={showConfirmPassword ? "text" : "password"}
                  value={confirmPassword}
                  required
                  onChange={handleConfirmPasswordChange}
                  width="100%"
                  placeholder="Confirm Password"
                />
                <InputRightElement>
                  {showConfirmPassword ? (
                    <AiOutlineEyeInvisible onClick={() => setShowConfirmPassword(false)} />
                  ) : (
                    <AiOutlineEye onClick={() => setShowConfirmPassword(true)} />
                  )}
                </InputRightElement>
              </InputGroup>
            </FormControl>
            <Button type="submit" colorScheme="blue" mt="4" width="100%">
              Sign up
            </Button>
          </form>
        </Card>
      </Box>
    </Center>
  );
};

const RegisterForm = () => {
  return (
    <Box
      minH="100vh"
      bgGradient="linear(to-b, #0077B6, #1C3144)"
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      <Register />
    </Box>
  );
};

export default RegisterForm;
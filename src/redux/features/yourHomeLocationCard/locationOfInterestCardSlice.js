import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  WADMKK: "",
  WADMKC: "",
  WADMKD: "",
  WADMRW: "",
  from_origin: "",
  home_loc: "",
}

export const locationOfInterestCardSlice = createSlice({
  name: 'locationOfInterestCard',
  initialState,
  reducers: {
    changeLocationOfInterestValue: (state, action) => {
      console.log(action.payload, "action.payload")
      state.WADMKK = action.payload.WADMKK
      state.WADMKC = action.payload.WADMKC
      state.WADMKD = action.payload.WADMKD
      state.WADMRW = action.payload.WADMRW
      state.from_origin = action.payload.from_origin
      state.home_loc = action.payload.home_loc
    },
  },
})

export const { changeLocationOfInterestValue } = locationOfInterestCardSlice.actions

export default locationOfInterestCardSlice.reducer
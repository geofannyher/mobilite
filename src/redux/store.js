import { configureStore } from '@reduxjs/toolkit'
import locationOfInterestCardReducer from './features/yourHomeLocationCard/locationOfInterestCardSlice'

export const store = configureStore({
  reducer: {
    locationOfInterestCard: locationOfInterestCardReducer,
  },
})
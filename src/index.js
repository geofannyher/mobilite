import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { ChakraProvider, extendTheme } from "@chakra-ui/react";

import { store } from "./redux/store";
import { Provider } from 'react-redux'

import "@fontsource/poppins";
import { ThemeProvider } from "@emotion/react";

const theme = extendTheme({
  fonts: {
    body: `'Poppins'`,
  },
});

// const theme = extendTheme({ font })

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  // <ThemeProvider>
    <ChakraProvider theme={theme}>
      <BrowserRouter>
        <Provider store={store}>
          <App />
        </Provider>
      </BrowserRouter>
    </ChakraProvider>
  // </ThemeProvider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

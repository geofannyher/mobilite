import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

// import HomePage from './pages/HomePage';
import Hero from './Pages/Landing/Hero';
import ProfilePage from './Pages/Profile';
import Login from './Pages/Auth/Login';

import Dashboard from "./Pages/Dashboard/dashboard";
import Layout from "./Component/Layout/Layout";
import Register from "./Pages/Auth/Register";
import HomeLoc from "./Component/Layout/cards/HomeLoc";
import LocOfInterest from "./Component/Layout/cards/LocOfInterest";

function App() {
  return (
      <Routes>
        {/* authentification route */}
        <Route path='/login' element={<Login />} />
        <Route path='/register' element={<Register />} />


        {/* layout route */}
        <Route path="/" element={<Layout />}>
          <Route index element={<Hero />} />
          <Route path='/dashboard' element={<Dashboard />} />
          <Route path="/home-loc" element={<HomeLoc />} />
          <Route path="/loc-of-interest" element={<LocOfInterest />} />
          <Route
            path="/account"
            element={<ProfilePage />}
          />
          <Route
            path="/login"
            element={<Login />}
          />
        </Route>
      </Routes>
  );
}

export default App;
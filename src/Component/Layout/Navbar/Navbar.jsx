import React, { useState } from 'react'
import { CgProfile, CgLogOut } from 'react-icons/cg'
import { TbSettings } from 'react-icons/tb'
import { SearchIcon } from '@chakra-ui/icons'
import './Navbar.css'
import {
    Box,
    Flex,
    Button,
    useColorModeValue,
    Stack,
    useColorMode,
    Show,
    HStack,
    Text,
    useDisclosure,
    IconButton,
    Hide,
    Link,
    Input,
    InputGroup,
    InputLeftElement,
    Center
} from "@chakra-ui/react";
import {
    MoonIcon,
    SunIcon,
    HamburgerIcon,
    CloseIcon,
    AddIcon,
} from "@chakra-ui/icons";
import ProfilePopUp from '../ProfilePopUp/ProfilePopUp'

const Nav = () => {
    let [profilePopUp, setProfilePopUp] = useState(false);

    const { colorMode, toggleColorMode } = useColorMode();
    const { isOpen, onOpen, onClose } = useDisclosure();

    const handleProfilePopUp = () => {
        setProfilePopUp(value => !value);
    }

    return (
        <div id="navFix">
            <Box
                bg={useColorModeValue("#D9D9D9", "gray.900")}
                px={10}
                width='100%'
            >
                <Flex h={16} alignItems={"center"} justifyContent={"space-between"}>
                    <HStack w="40%" marginLeft="390px">
                    <InputGroup boxShadow="0px 4px 25px #F5F5F5">
                        <InputLeftElement
                            pointerEvents="none"
                            children={<SearchIcon color="black.300" />}
                        />
                        <Input type="text" placeholder="Search place" />
                    </InputGroup>


                    </HStack>

                    {/* menu navbar */}
                    {/* <Flex h={16} alignItems={"center"} justifyContent={"space-between"}>
                        <HStack spacing={8} alignItems={"center"}>
                            <HStack
                                as={"nav"}
                                spacing={4}
                                display={{ base: "none", md: "flex" }}
                                id="myDIV"
                            >
                                <Button className="btnRes">
                                    <a href="#Contact">
                                        <CgProfile />
                                    </a>
                                </Button>
                            </HStack>
                        </HStack>
                    </Flex> */}

                    {/* badge navbar */}
                    <Flex alignItems={"center"}>
                        <Stack direction={"row"} spacing={2}>
                            <Button className="btnRes" isActive={false}>
                                <Link>
                                    <CgLogOut />
                                </Link>
                            </Button>
                            <Button className="btnRes">
                                <Link >
                                    <TbSettings />
                                </Link>
                            </Button>
                            <Button className="btnRes" onClick={()=>handleProfilePopUp()} position="relative">
                                <Link>
                                    <CgProfile />
                                    {profilePopUp && <ProfilePopUp />}
                                </Link>
                            </Button>
                            <Button onClick={toggleColorMode}>
                                {colorMode === "light" ? <MoonIcon /> : <SunIcon />}
                            </Button>
                        </Stack>
                    </Flex>
                    <IconButton
                        size={"md"}
                        icon={isOpen ? <CloseIcon /> : <HamburgerIcon />}
                        aria-label={"Open Menu"}
                        display={{ md: "none" }}
                        onClick={isOpen ? onClose : onOpen}
                    />
                    {isOpen ? (
                        <Box pb={4} display={{ md: "none" }}>
                            <Stack as={"nav"} spacing={4}>
                                <Button
                                    onClick={isOpen ? onClose : onOpen}
                                    _hover={{
                                        textShadow: "#FC0 1px 0 10px",
                                        transform: "scale(1.2)",
                                    }}
                                >
                                    <a href="#Contact">
                                        <p>Account</p>
                                    </a>
                                </Button>
                            </Stack>
                        </Box>
                    ) : null}
                </Flex>
            </Box>
        </div>
    );
}

export default Nav;

import React from "react";
import Nav from "./Navbar/Navbar";
import { Outlet } from "react-router-dom";
import HomeLoc from "./cards/HomeLoc";

const Layout = () => {
  return (
    <div>
      <Nav />
      <Outlet />
    </div>
  );
};

export default Layout;

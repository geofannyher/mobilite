import React from 'react'
import { Box, Button, Image, Text } from '@chakra-ui/react';

const ProfilePopUp = () => {
  const handleLogOut = () => {
    sessionStorage.removeItem("data");
  }

  return (
    <Box 
      bgColor="#9291A5" 
      w="238px" 
      borderRadius="20px" 
      opacity={0.9} 
      position="absolute" 
      display="flex" 
      flexDirection="column" 
      padding="30px"
      right="0"
      gap="10px"
      >
      <Box display="flex" alignItems="center" gap="20px" color="#F5F5F5">
        <Image src='https://bit.ly/dan-abramov' alt='Dan Abramov' w="45px" h="45px" borderRadius="920px" />
        <Text>Ini Profile</Text>
      </Box>
      <Box display="flex" flexDir="column" alignItems="center" gap="10px">
        <Button w="190px" borderRadius="30px" bgColor="#D9D9D9" color="#000000" p="5px" onClick={console.log("clicked")}>Profile</Button>
        <Button w="190px" borderRadius="30px" bgColor="#D9D9D9" color="#000000" p="5px">Pricing</Button>
        <Button w="190px" borderRadius="30px" bgColor="#D9D9D9" color="#000000" p="5px" onClick={handleLogOut()}>Logout</Button>
      </Box>
    </Box>
  )
}

export default ProfilePopUp;
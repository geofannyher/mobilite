import React from "react";
import { HiLocationMarker } from "react-icons/hi";
import {
  Progress,
  Box,
  Text,
  Icon,
  Card,
  Stack,
  StackDivider,
  Center,
  HStack,
  VStack,
  Grid,
  GridItem,
} from "@chakra-ui/react";

import { useSelector } from 'react-redux'

const LocOfInterest = () => {
  const locationOfInterestCard = useSelector((state) => state.locationOfInterestCard);

  const boxStyle = {
    maxW: "100%",
    // m: 5,
  };
  const textHeader = {
    fontSize: 20,
    fontWeight: "black",
    color: "gray.700",
    fontStyle: "italic",
    mb: 3,
  };
  const textCardContent = {
    fontSize: 12,
  };
  const card = {
    ps: 5,
    pe: 10,
    py: 2,
    boxShadow: "md",
    w: "85%",
    mb: 3,
  };
  const CircleIcon = (props) => (
    <Icon viewBox="0 0 200 200" {...props}>
      <path
        fill="currentColor"
        d="M 100, 100 m -75, 0 a 75,75 0 1,0 150,0 a 75,75 0 1,0 -150,0"
      />
    </Icon>
  );
  return (
    <Box sx={boxStyle}>
      <Box bg={"#F8F8F8"} boxShadow="md" borderRadius={"20px"}>
        <Box p={5}>
          <Text sx={textHeader}>Location Of Interest</Text>
          <Stack direction="row" mb={3} spacing={5}>
            <Icon as={HiLocationMarker} w={6} h={6} color="blue.500" />
            <Card sx={card}>
              <Text sx={textCardContent} fontWeight={"bold"}>
                {locationOfInterestCard.WADMKK}
              </Text>
              <Text sx={textCardContent} fontWeight={"bold"}>
                {locationOfInterestCard.WADMKC}
              </Text>
              <Text sx={textCardContent}>{locationOfInterestCard.WADMKD} RW {locationOfInterestCard.WADMRW}</Text>
            </Card>
          </Stack>
          <Text sx={textHeader}>Transport Cost</Text>
          <Stack direction="column" mb={"3"}>
            <Text sx={textCardContent}>Transit</Text>
            <Progress
              colorScheme="blue"
              height={"20px"}
              value={5}
              borderRadius="md"
            />
            <Text sx={textCardContent}>Driving</Text>
            <Progress
              colorScheme="yellow"
              height={"20px"}
              value={15}
              borderRadius="md"
            />
            <Text sx={textCardContent}>Ridehail</Text>
            <Progress
              colorScheme="gray"
              height={"20px"}
              value={70}
              borderRadius="md"
            />
          </Stack>
          <Text sx={textHeader}>CO2e Emission</Text>
          <Grid templateColumns="repeat(6, 1fr)" gap={4} mb={"3"}>
            <GridItem colSpan={3}>
              <Progress
                colorScheme="blue"
                size="md"
                h={"15px"}
                value={100}
                borderRadius={"md"}
              />
            </GridItem>
            <GridItem colSpan={3} colorScheme={"#FFD968"}>
              <Progress
                colorScheme="yellow"
                size="md"
                h={"15px"}
                value={60}
                borderRadius={"md"}
              />
            </GridItem>
          </Grid>
          <Box
            borderRadius={"30px"}
            boxShadow={"3"}
            bg={"gray.300"}
            ps={"7px"}
            py={"1"}
            mb={"3"}
          >
            <Stack direction={"row"}>
              <CircleIcon boxSize={3} color={"#5289BA"} />
              <Text>Transit</Text>
            </Stack>
          </Box>

          <Box
            borderRadius={"30px"}
            boxShadow={"3"}
            bg={"gray.300"}
            ps={"7px"}
            py={"1"}
            mb={"3"}
          >
            <Stack direction={"row"}>
              <CircleIcon boxSize={3} color={"#FFD968"} />
              <Text>Driving</Text>
            </Stack>
          </Box>

          <Text sx={textHeader}>Housing Transport Cost</Text>
          <Card p={2}>
            <Stack divider={<StackDivider />} direction={"row"}>
              <Box>
                <Text sx={textCardContent}>Housing</Text>
                <Text fontSize={"14px"} fontWeight={"bold"}>
                  IDR 757K/month
                </Text>
              </Box>
              <Box>
                <Text sx={textCardContent}>100% Transit</Text>

                <Text fontSize={"14px"} fontWeight={"bold"}>
                  IDR 381,2K/month
                </Text>
              </Box>
            </Stack>
            {/* <Stack divider={<StackDivider />} direction={"row"}>  
            </Stack> */}
          </Card>
        </Box>
        <Box bg={"#5289BA"} borderBottomRadius={"20px"}>
          <Center>
            <Stack direction="column" py={"2"}>
              <Text sx={textCardContent} color={"white"}>
                Lower than your home location
              </Text>
              <Text sx={textCardContent} color={"white"}>
                Daily commute to this location
              </Text>
            </Stack>
          </Center>
        </Box>
      </Box>
    </Box>
  );
};

export default LocOfInterest;

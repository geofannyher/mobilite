import React from "react";
import { FaHome } from "react-icons/fa";
import { Progress, Box, Text, Icon, Card, Stack } from "@chakra-ui/react";

const HomeLoc = () => {
  const boxStyle = {
    maxW: "100%",
    // m: 5,
  };
  const textHeader = {
    fontSize: 20,
    fontWeight: "black",
    color: "gray.700",
    fontStyle: "italic",
    mb: 3,
  };
  const textCardContent = {
    fontSize: 12,
  };
  const card = {
    ps: 5,
    pe: 10,
    py: 2,
    boxShadow: "md",
    w: "85%",
    mb: 2,
  };

  return (
    <Box sx={boxStyle}>
      <Box bg={"#F8F8F8"} p={5} boxShadow="md" borderRadius={"20px"}>
        <Text sx={textHeader}>Your Home Location</Text>
        <Stack direction="row" mb={3} spacing={5}>
          <Icon as={FaHome} w={5} h={5} />
          <Card sx={card}>
            <Text sx={textCardContent} fontWeight={"bold"}>
              JAKARTA PUSAT
            </Text>
            <Text sx={textCardContent} fontWeight={"bold"}>
              GAMBIR
            </Text>
            <Text sx={textCardContent}>GAMBIR RW 002</Text>
          </Card>
        </Stack>
        <Stack direction="column">
          <Text sx={textCardContent}>Estimated Housing Cost</Text>
          <Progress
            colorScheme="blue"
            height={"20px"}
            value={30}
            borderRadius="md"
          />
          <Text sx={textCardContent}>Housing (Month)</Text>
          <Progress
            colorScheme="yellow"
            height={"20px"}
            value={80}
            borderRadius="md"
          />
          <Text sx={textCardContent}>
            Estimated Transport Accessiblity Score
          </Text>
          <Progress
            colorScheme="green"
            height={"20px"}
            value={0}
            borderRadius="md"
          />
        </Stack>
      </Box>
    </Box>
  );
};

export default HomeLoc;
